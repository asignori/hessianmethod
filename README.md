# HessianMethod

Implementation of the hessian method to calculate relative uncertainties based on collinear distribution functions.
Calculations have been performed with full average kinematics (see the documentation for more details). 
Accordingly, only the calculation of the relative hessian error is reliable. To get the absolute error it is recommended to multiply the relative error by the experimental measurement, not the prediction at the average kinematics.

Dependencies: 
- Nanga Parbat (https://github.com/MapCollaboration/NangaParbat) [tested with "Beta" version - private]
- dynnlo (https://www.physik.uzh.ch/en/groups/grazzini/research/Tools.html) has been used separately to compute fiducial cross sections for Drell-Yan/Z production

Available observables: 
- Drell-Yan/Z: dsigma/dqT and 1/sigma*dsigma/dqT
- SIDIS: TMD multiplicities M(x,z,|PhT|^n,Q), n=1,2

Notes:
- see "doc.pdf" for explanatory material related to the calculation of the observables
- the source code is in "src"; check the Makefile for possible user-dependent paths
