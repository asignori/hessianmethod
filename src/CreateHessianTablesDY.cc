/*
 Author: Chiara Bissolotti: chiara.bissolotti01@universitadipavia.it
*/

#include "NangaParbat/fastinterface.h"
#include "NangaParbat/convolutiontable.h"
#include "NangaParbat/bstar.h"

#include <fstream>
#include <cstring> // for linux


//------------------------------------------------------------------------------
// Main program
int main(int argc, char* argv[])
{
  // Check that the input is correct otherwise stop the code
  if(argc < 4 || strcmp(argv[1], "--help") == 0)
    {
      std::cout << std::endl;
      std::cout << "Invalid Parameters:" << std::endl;
      std::cout << "Syntax: ./CreateHessianTables < 1 - configuration file> < 2 - path to data folder> < 3 - output folder for tables> " << std::endl;
      std::cout << std::endl;
      exit(-10);
    }

  // Open the configuration file and retrieve the PerturbativeOrder (to print it in the plot)
  YAML::Node config = YAML::LoadFile(argv[1]);
  //std::cout << "check 1" << std::endl;

  // Open datasets.yaml file that contains the list of tables to be produced and push data sets into the a vector of DataHandler objects.
  const YAML::Node datasets = YAML::LoadFile(std::string(argv[2]) + "/datasets_DY_fixtar_E772.yaml");
  //std::cout << "check 2" << std::endl;

  // Allocate and fill DataHandler object going through datafiles
  std::vector<NangaParbat::DataHandler> DHVect;
  for (auto const& pred : datasets)
    for (auto const& ds : pred.second)
      {
        const std::string datafile = std::string(argv[2]) + "/" + pred.first.as<std::string>() + "/" + ds["file"].as<std::string>();
        DHVect.push_back(NangaParbat::DataHandler{ds["name"].as<std::string>(), YAML::LoadFile(datafile)});
      }

  //std::cout << "check 3" << std::endl;

  // number of files present in the PDF folder
  // the number of hessian members in MMHT2014nlo68cl is 50 + 1 (the 0th member is the central value)
  const int nhesm = 51;

  // Allocate "FastInterface" object reading the parameters from an input card.
  for (size_t i = 0; i < nhesm; i++) // MMHT2014nlo68cl has 50 elements + the 0th, central value
    {
      config["pdfset"]["member"] = i;
      //std::cout << "check 3.5" << std::endl;
      const NangaParbat::FastInterface FIObj{config};
    
      //std::cout << "check 4" << std::endl;

      // Compute tables
      //const std::vector<YAML::Emitter> Tabs = FIObj.ComputeTables(DHVect, NangaParbat::bstarmin);
      //const std::vector<YAML::Emitter> Tabs = FIObj.ComputeTables(DHVect);
      const std::vector<std::string> Tabs = FIObj.ComputeTables(DHVect);

      //std::cout << "check 5" << std::endl;

      // Dump table to file
      for (auto const& tab : Tabs)
      {
        std::ofstream fout(std::string(argv[3]) + YAML::Load(tab.c_str())["name"].as<std::string>() + "_" + std::to_string(i) + ".yaml");
        //std::cout << "check 6" << std::endl;
        fout << tab.c_str() << std::endl;
        //std::cout << "check 7" << std::endl;
        fout.close();
      }
    }

  return 0;
}
