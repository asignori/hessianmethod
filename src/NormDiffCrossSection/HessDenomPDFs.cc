/*
 author: Chiara Bissolotti - chiara.bissolotti01@universitadipavia.it
*/


#include "NangaParbat/DWS.h"
#include "NangaParbat/PV17.h"
#include "NangaParbat/PV19.h"
#include "NangaParbat/fastinterface.h"
#include "NangaParbat/convolutiontable.h"
//#include "NangaParbat/utilities.h"
#include "NangaParbat/bstar.h"

#include <iostream>
#include <fstream>
#include <sys/stat.h>
#include <cstring>

#include <apfel/timer.h>



//------------------------------------------------------------------------------
// Main program
int main(int argc, char* argv[])
{
  // Check that the input is correct otherwise stop the code
  if(argc < 6 || strcmp(argv[1], "--help") == 0)
    {
      std::cout << std::endl;
      std::cout << "Invalid Parameters:" << std::endl;
      std::cout << "Syntax: ./HessDenomPDFs <1 - configuration file> <2 - path to data folder> <3 - path to tables folder> <4 - path to denom.folder> <5 - path to output>" << std::endl;
      std::cout << std::endl;
      exit(-10);
    }

    // Open the configuration file
    YAML::Node config = YAML::LoadFile(argv[1]);

    // Retrieve the PerturbativeOrder
    std::string pertord = config["PerturbativeOrder"].as<std::string>();
    std::string stringord = "please specify";
    if (pertord == "1")
      stringord = "NLL";
    if (pertord == "2")
      stringord = "NNLL";
    if (pertord == "3")
      stringord = "N3LL";
    if (pertord == "-1")
      stringord = "NLLp";
    if (pertord == "-2")
      stringord = "NNLLp";

  // Open datasets.yaml file that contains the list of tables to be produced
  const YAML::Node datasets = YAML::LoadFile(std::string(argv[2]) + "/datasets_nxsec_combined.yaml");

  // number of files present in the PDF folder
  // the number of hessian members in MMHT2014nlo68cl is 50 + 1 (the 0th member is the central value)
  const int nhesm = 51;

  // Allocate "Parameterisation" derived object
  // NangaParbat::PV19 NPFunc{};
  NangaParbat::PV17 NPFunc{};

  const std::string NPfuncName = NPFunc.GetName();

  // Non-perturbative function
  auto const fNP = [&] (double const& x, double const& b, double const& zeta, int const& ifun) -> double{ return NPFunc.Evaluate(x, b, zeta, ifun); };

  // Now read convolution tables and compute predictions from the grids
  for (auto const& exp : datasets)
    for (auto const& ds : exp.second)
      {
        // Fill DataHandler object dh
        const std::string datafile = std::string(argv[2]) + "/" + exp.first.as<std::string>() + "/" + ds["file"].as<std::string>();
        const NangaParbat::DataHandler dh{ds["name"].as<std::string>(), YAML::LoadFile(datafile)};

        // Get qT points and experimental values
        const std::vector<double> qT = dh.GetKinematics().qTv;
        const std::vector<double> mean = dh.GetMeanValues();

        // Fill gc vector with the prediction from each hessian table.
        // Predictions NOT divided for the cross section, the prefactor must be one for all the convolution tables.
        std::vector<std::vector<double>> gc;
        for (size_t i = 0; i < nhesm ; i++)
        {
          const std::string table = std::string(argv[3]) + "/" + ds["name"].as<std::string>() + "_" + std::to_string(i) + ".yaml";
          const NangaParbat::ConvolutionTable ct{YAML::LoadFile(table)};
          gc.push_back(ct.GetPredictions(fNP));
        }

        // Load denominator file (calculated with DY@NNLO)
        const double cols = 2;
      	std::fstream file;
      	std::vector<std::vector<double>> sigtot; // 2d array as a vector of vectors
      	std::vector<double> rowVector(cols); // vector to add into 'array' (represents a row)
      	int row = 0; // Row counter
        // Read file
        std::string flname;
        if (pertord == "2" || pertord == "-1")
          flname = std::string(argv[4]) + "/" + "sigma_" + ds["name"].as<std::string>() + "_nlo.dat"; // change this name
        if (pertord == "3" || pertord == "-2")
          flname = std::string(argv[4]) + "/" + "dynnlo_hessian_sets_MMHTnnlo_" + ds["name"].as<std::string>() + ".dat";

        file.open(flname, std::ios::in); // Open file
        // the 1st column ([0]) is the sigma and the 2nd ([1])the errors
      	if (file.is_open())
          {
        		std::cout << "File opened correctly" << std::endl;
        		// Dynamically store data into array
        		while (file.good())
              {
        			  sigtot.push_back(rowVector); // add a new row,
        			  for (int col = 0; col < cols; col++)
        				  file >> sigtot[row][col]; // fill the row with col elements
        			  row++; // Keep track of actual row
        		  }
        	}
        file.close();

        //Adjusting unit of measure
        std::vector<double> SigmaTot(row ,0.);
        for (size_t i = 0; i < row; i++)
          SigmaTot[i] = sigtot[i][0] * 1e-3 ;

        // Observable
        // dsigma/dqT / SigmaTot (see documentation for the calculation at average kinematics)
        double obs [(int) gc.size()][(int) gc[0].size()];
        memset( obs, 0, gc.size()* gc[0].size() * sizeof(double) ); // Sets the first num bytes of the block of memory pointed by ptr to the specified value

        for (int j = 0; j < (int) gc[0].size(); j++) // j index on the qT points
            for (int i = 0; i < (int) gc.size(); i++) // i index on the tables
              /*
              if (ds["name"].as<std::string>() == "D0_RunII" || ds["name"].as<std::string>() == "D0_RunIImu")
                obs[i][j] = gc[i][j] / SigmaTot[i]; // !! no mult. x2 in the D0 RunII case
              else if (ds["name"].as<std::string>() == "ATLAS_8TeV_Q_46_66")
                obs[i][j] = gc[i][j] * 2 * 20 / SigmaTot[i]; // 20 = 66 - 46, the DeltaQ
              else if (ds["name"].as<std::string>() == "ATLAS_8TeV_Q_116_150")
                obs[i][j] = gc[i][j] * 2 * 34 / SigmaTot[i]; // 20 = 66 - 46, the DeltaQ
              else
                obs[i][j] = gc[i][j] * 2 / SigmaTot[i]; // !! * 2 (for the interval in rapidity)
              */
                // if one is interested in the relative hessian error then this is the only relevant line, since all constant prefactors cancel in ratios
                obs[i][j] = gc[i][j] / SigmaTot[i]; 

        // Apply the Hessian formula
        std::vector<double> dgctot(gc[0].size(), 0.); // initializing vector for the errors DX - size of the number of the points, all elements initialized at zero
        std::vector<double> relerrtot(gc[0].size(), 0.); // relative hessian error
        for (int j = 0; j < (int) gc[0].size(); j++) // j index on the qT points
          {
            for (int i = 1; i < (int) gc.size() / 2; i++) // i index on the tables
              dgctot[j] += pow(obs[2*i-1][j] - obs[2*i][j], 2);
            dgctot[j] = sqrt(dgctot[j]) / 2;
            relerrtot[j] = dgctot[j]/ obs[0][j];
          }

        // Create directory
        std::string path = std::string(argv[5]) + "/" + NPfuncName;
        mkdir(path.c_str(), ACCESSPERMS);

        // Print on file.out
        std::ofstream newdatafile(path + "/" + ds["name"].as<std::string>() + ".out", std::ios::out);
        newdatafile << "# Dataset name: " << ds["name"].as<std::string>() << "\t"
                    //<< " Non pert.func. = " << NPfuncName
                    << std::endl;
        newdatafile << "#\t"
                    << "  qT [GeV]   \t"
                    << "   pred.     \t"
                    << "    exp.     \t"
                    //<< " hes.abs.err \t" // unreliable when calculated with tables w/o integrations (see the documentation)
                    << "  hes.err(%) \t"
                    << "  hes.err * exp \t"
                    << std::endl;

        for (int i = 0; i < (int) dgctot.size(); i++)
          {
            // print zero (and not 'nan') if the prediction doesn't exist ( = 0) or it's negative
            if (obs[0][i] <=  0)
              {
                //obs[0][i] =  0;
                //dgctot[i] = 0;
                //relerrtot[i] = 0;
              }
              newdatafile << std::scientific;
              newdatafile << i << "\t"
                          << (dh.GetKinematics().IntqT ? ( qT[i] + qT[i+1] ) / 2 : qT[i]) << "\t"
                          << obs[0][i] << "\t"
                          << mean[i]  << "\t"
                          //<< dgctot[i]   << "\t" // unreliable when calculated with tables w/o integrations (see the documentation)
                          << relerrtot[i] << "\t"
                          << relerrtot[i] * mean[i] << "\t"
                          << std::endl;
          }

        newdatafile.close();

        // Compute number of significant predictions (to plot)
        int npred = 0; // number of predictions different from zero, = number of considered qT points
        for (int i = 0; i < (int) dgctot.size(); i++)
            if ( gc[0][i] >  0)
              npred++ ;

        // Print on terminal
        std::cout << "# Dataset name: " << ds["name"].as<std::string>() << "\t"
                  << " Non pert.func. = " << NPfuncName << "\t"
                  << " pert. ord. = " << stringord
                  << std::endl;
        std::cout << "#\t"
                    << "  qT [GeV]   \t"
                    << "   pred.     \t"
                    << "    exp.     \t"
                    //<< " hes.abs.err \t" // unreliable when calculated with tables w/o integrations (see the documentation)
                    << "  hes.err(%) \t"
                    << "  hes.err * exp \t"
                    << std::endl;

        for (int i = 0; i < (int) dgctot.size(); i++)
          {
            //if (gc[0][i] > 0) // print only if the prediction is > 0
            //{
              std::cout << std::scientific;
              std::cout << i << "\t"
                        << (dh.GetKinematics().IntqT ? ( qT[i] + qT[i+1] ) / 2 : qT[i]) << "\t"
                        << obs[0][i] << "\t"
                        << mean[i]  << "\t"
                        //<< dgctot[i]   << "\t" // unreliable when calculated with tables w/o integrations (see the documentation)
                        << relerrtot[i] << "\t"
                        << relerrtot[i] * mean[i] << "\t"
                        << std::endl;
            //}
          }

      }

  return 0;
}
