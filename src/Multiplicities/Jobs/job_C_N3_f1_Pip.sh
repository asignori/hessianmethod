#!/bin/bash 
#SBATCH --job-name=C3f1Pip
#SBATCH -p standard
#SBATCH -o out_C_N3_f1_Pip.txt
#SBATCH -e err_C_N3_f1_Pip.txt

exe=HessianPDFs_SIDIS_C_hp
config=../hessconfig_N3LL.yaml
data=../../data_SIDIS/ 
tables=../../../tables/SIDIS/COMPASS/N3LL/no_integrations/Pip_component/
outp=output_C_N3LL_f1/Pip_component

./$exe $config $data $tables $outp
