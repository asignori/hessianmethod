#!/bin/bash 
#SBATCH --job-name=H2D1Pi
#SBATCH -p standard
#SBATCH -o out_H_N2_D1_Pi.txt
#SBATCH -e err_H_N2_D1_Pi.txt

exe=HessianFFs_H_Pi
config=../hessconfig_N2LL.yaml
data=../../data_SIDIS/ 
tables=../../../tables/SIDIS/HERMES/N2LL/no_integrations/
outp=output_H_N2LL_D1_Pi/

./$exe $config $data $tables $outp
