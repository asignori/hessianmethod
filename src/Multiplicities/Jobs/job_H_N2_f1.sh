#!/bin/bash 
#SBATCH --job-name=H2f1
#SBATCH -p standard
#SBATCH -o out_H_N2_f1.txt
#SBATCH -e err_H_N2_f1.txt

exe=HessianPDFs_SIDIS_H
config=../hessconfig_N2LL.yaml
data=../../data_SIDIS/ 
tables=../../../tables/SIDIS/HERMES/N2LL/no_integrations/
outp=output_H_N2LL_f1/

./$exe $config $data $tables $outp
