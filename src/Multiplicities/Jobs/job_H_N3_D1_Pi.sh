#!/bin/bash 
#SBATCH --job-name=H3D1Pi
#SBATCH -p standard
#SBATCH -o out_H_N3_D1_Pi.txt
#SBATCH -e err_H_N3_D1_Pi.txt

exe=HessianFFs_H_Pi
config=../hessconfig_N3LL.yaml
data=../../data_SIDIS/ 
tables=../../../tables/SIDIS/HERMES/N3LL/no_integrations/
outp=output_H_N3LL_D1_Pi/

./$exe $config $data $tables $outp
