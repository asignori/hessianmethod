#!/bin/bash 
#SBATCH --job-name=H3D1K
#SBATCH -p standard
#SBATCH -o out_H_N3_D1_K.txt
#SBATCH -e err_H_N3_D1_K.txt

exe=HessianFFs_H_K
config=../hessconfig_N3LL.yaml
data=../../data_SIDIS/ 
tables=../../../tables/SIDIS/HERMES/N3LL/no_integrations/
outp=output_H_N3LL_D1_K/

./$exe $config $data $tables $outp
