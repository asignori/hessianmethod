#!/bin/bash 
#SBATCH --job-name=H2D1K
#SBATCH -p standard
#SBATCH -o out_H_N2_D1_K.txt
#SBATCH -e err_H_N2_D1_K.txt

exe=HessianFFs_H_K
config=../hessconfig_N2LL.yaml
data=../../data_SIDIS/ 
tables=../../../tables/SIDIS/HERMES/N2LL/no_integrations/
outp=output_H_N2LL_D1_K/

./$exe $config $data $tables $outp
