#!/bin/bash 
#SBATCH --job-name=C3D1Pim
#SBATCH -p standard
#SBATCH -o out_C_N3_D1_Pim.txt
#SBATCH -e err_C_N3_D1_Pim.txt

exe=HessianFFs_C_Pim
config=../hessconfig_N3LL.yaml
data=../../data_SIDIS/ 
tables=../../../tables/SIDIS/COMPASS/N3LL/no_integrations/Pim_component/
outp=output_C_N3LL_D1/Pim_component

./$exe $config $data $tables $outp
