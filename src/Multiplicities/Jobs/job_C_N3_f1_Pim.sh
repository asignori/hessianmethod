#!/bin/bash 
#SBATCH --job-name=C3f1Pim
#SBATCH -p standard
#SBATCH -o out_C_N3_f1_Pim.txt
#SBATCH -e err_C_N3_f1_Pim.txt

exe=HessianPDFs_SIDIS_C_hm
config=../hessconfig_N3LL.yaml
data=../../data_SIDIS/ 
tables=../../../tables/SIDIS/COMPASS/N3LL/no_integrations/Pim_component/
outp=output_C_N3LL_f1/Pim_component

./$exe $config $data $tables $outp
