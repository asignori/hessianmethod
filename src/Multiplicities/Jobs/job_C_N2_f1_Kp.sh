#!/bin/bash 
#SBATCH --job-name=C2f1Kp
#SBATCH -p standard
#SBATCH -o out_C_N2_f1_Kp.txt
#SBATCH -e err_C_N2_f1_Kp.txt

exe=HessianPDFs_SIDIS_C_hp
config=../hessconfig_N2LL.yaml
data=../../data_SIDIS/ 
tables=../../../tables/SIDIS/COMPASS/N2LL/no_integrations/Kp_component/
outp=output_C_N2LL_f1/Kp_component

./$exe $config $data $tables $outp
