#!/bin/bash 
#SBATCH --job-name=C2D1Km
#SBATCH -p standard
#SBATCH -o out_C_N2_D1_Km.txt
#SBATCH -e err_C_N2_D1_Km.txt

exe=HessianFFs_C_Km
config=../hessconfig_N2LL.yaml
data=../../data_SIDIS/ 
tables=../../../tables/SIDIS/COMPASS/N2LL/no_integrations/Km_component/
outp=output_C_N2LL_D1/Km_component

./$exe $config $data $tables $outp
