#!/bin/bash 
#SBATCH --job-name=H3f1
#SBATCH -p standard
#SBATCH -o out_H_N3_f1.txt
#SBATCH -e err_H_N3_f1.txt

exe=HessianPDFs_SIDIS_H
config=../hessconfig_N3LL.yaml
data=../../data_SIDIS/ 
tables=../../../tables/SIDIS/HERMES/N3LL/no_integrations/
outp=output_H_N3LL_f1/

./$exe $config $data $tables $outp
