#!/bin/bash 
#SBATCH --job-name=C3D1Pip
#SBATCH -p standard
#SBATCH -o out_C_N3_D1_Pip.txt
#SBATCH -e err_C_N3_D1_Pip.txt

exe=HessianFFs_C_Pip
config=../hessconfig_N3LL.yaml
data=../../data_SIDIS/ 
tables=../../../tables/SIDIS/COMPASS/N3LL/no_integrations/Pip_component/
outp=output_C_N3LL_D1/Pip_component

./$exe $config $data $tables $outp
