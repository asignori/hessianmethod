#!/bin/bash 
#SBATCH --job-name=C3f1Km
#SBATCH -p standard
#SBATCH -o out_C_N3_f1_Km.txt
#SBATCH -e err_C_N3_f1_Km.txt

exe=HessianPDFs_SIDIS_C_hm
config=../hessconfig_N3LL.yaml
data=../../data_SIDIS/ 
tables=../../../tables/SIDIS/COMPASS/N3LL/no_integrations/Km_component/
outp=output_C_N3LL_f1/Km_component

./$exe $config $data $tables $outp
