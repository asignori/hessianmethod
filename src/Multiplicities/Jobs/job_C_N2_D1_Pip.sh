#!/bin/bash 
#SBATCH --job-name=C2D1Pip
#SBATCH -p standard
#SBATCH -o out_C_N2_D1_Pip.txt
#SBATCH -e err_C_N2_D1_Pip.txt

exe=HessianFFs_C_Pip
config=../hessconfig_N2LL.yaml
data=../../data_SIDIS/ 
tables=../../../tables/SIDIS/COMPASS/N2LL/no_integrations/Pip_component/
outp=output_C_N2LL_D1/Pip_component

./$exe $config $data $tables $outp
