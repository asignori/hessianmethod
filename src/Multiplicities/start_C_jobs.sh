## Compass, N3LL, D1
sbatch Jobs/job_C_N3_D1_Pip.sh
sbatch Jobs/job_C_N3_D1_Pim.sh
sbatch Jobs/job_C_N3_D1_Kp.sh
sbatch Jobs/job_C_N3_D1_Km.sh

## Compass, N2LL, D1
sbatch Jobs/job_C_N2_D1_Pip.sh
sbatch Jobs/job_C_N2_D1_Pim.sh
sbatch Jobs/job_C_N2_D1_Kp.sh
sbatch Jobs/job_C_N2_D1_Km.sh

## Compass, N3LL, f1
sbatch Jobs/job_C_N3_f1_Pip.sh
sbatch Jobs/job_C_N3_f1_Pim.sh
sbatch Jobs/job_C_N3_f1_Kp.sh
sbatch Jobs/job_C_N3_f1_Km.sh

## Compass, N2LL, f1
sbatch Jobs/job_C_N2_f1_Pip.sh
sbatch Jobs/job_C_N2_f1_Pim.sh
sbatch Jobs/job_C_N2_f1_Kp.sh
sbatch Jobs/job_C_N2_f1_Km.sh

