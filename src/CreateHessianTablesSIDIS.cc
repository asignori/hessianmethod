/*
 Author: Chiara Bissolotti: chiara.bissolotti01@universitadipavia.it
*/

#include "NangaParbat/fastinterface.h"
#include "NangaParbat/convolutiontable.h"
#include "NangaParbat/bstar.h"

#include <fstream>
#include <cstring> // for linux


//------------------------------------------------------------------------------
// Main program
int main(int argc, char* argv[])
{
  // Check that the input is correct otherwise stop the code
  if(argc < 4 || strcmp(argv[1], "--help") == 0)
    {
      std::cout << std::endl;
      std::cout << "Invalid Parameters:" << std::endl;
      std::cout << "Syntax: ./CreateHessianTablesSIDIS < 1 - configuration file> < 2 - path to data folder> < 3 - output folder for tables> " << std::endl;
      std::cout << std::endl;
      exit(-10);
    }

  // Open the configuration file
  YAML::Node config = YAML::LoadFile(argv[1]);

  // it would be good to retrieve pert. order here

  // Open SIDISdatasets.yaml file that contains the list of tables to be produced and push data sets into the a vector of DataHandler objects
  const YAML::Node datasets = YAML::LoadFile(std::string(argv[2]) + "/datasets_C_hm.yaml");

  // Allocate and fill DataHandler object going through datafiles
  std::vector<NangaParbat::DataHandler> DHVect;
  for (auto const& pred : datasets)
    for (auto const& ds : pred.second)
      {
        const std::string datafile = std::string(argv[2]) + "/" + pred.first.as<std::string>() + "/" + ds["file"].as<std::string>();
        DHVect.push_back(NangaParbat::DataHandler{ds["name"].as<std::string>(), YAML::LoadFile(datafile)});
      }

  // number of files present in the PDF & FF folders
  // MMHT2014*lo68cl: the number of hessian members is 50 + 1 (the 0th member is the central value)
  const int nhesmPDF = 51; //7;
  // DSS14_NLO_Pi*: the number of hessian members is 56 + 1 (the 0th member is the central value)
  // DSS17_NLO_Kaon*: the number of hessian members is 40 + 1 (the 0th member is the central value)
  // implement if statements to choose FFset according to final-state hadron
  const int nhesmFF = 41; 

  // Allocate "FastInterface" object reading the parameters from an input card
  // Loop over PDF members
  for (size_t i = 0; i < nhesmPDF; i++) 
    {
      config["pdfset"]["member"] = i;
      const NangaParbat::FastInterface FIObj{config};
    
      // Compute tables
      const std::vector<std::string> Tabs = FIObj.ComputeTables(DHVect);

      // Dump table to file
      for (auto const& tab : Tabs)
      {
        std::ofstream fout(std::string(argv[3]) + YAML::Load(tab.c_str())["name"].as<std::string>() + "_PDFmem_" + std::to_string(i) + "_FFmem_0" + ".yaml");
        fout << tab.c_str() << std::endl;
        fout.close();
      }
    }

  // Loop over FF members
  config["pdfset"]["member"] = 0; // re-set PDF member to 0
  // start from 1 since we already have the (0,0) case
  for (size_t i = 1; i < nhesmFF; i++)  
    {
      config["ffset"]["member"] = i;
      const NangaParbat::FastInterface FIObj{config};
    
      // Compute tables
      const std::vector<std::string> Tabs = FIObj.ComputeTables(DHVect);

      // Dump table to file
      for (auto const& tab : Tabs)
      {
        std::ofstream fout(std::string(argv[3]) + YAML::Load(tab.c_str())["name"].as<std::string>() + "_PDFmem_0_FFmem_" + std::to_string(i) + ".yaml");
        fout << tab.c_str() << std::endl;
        fout.close();
      }
    }

  return 0;
}
