#!/bin/bash
## running parallel jobs on jefferson

sbatch Jobs_fixtar/job_fixtar_E288_N3LL.sh
sbatch Jobs_fixtar/job_fixtar_E288_N2LL.sh

sbatch Jobs_fixtar/job_fixtar_E605_N3LL.sh
sbatch Jobs_fixtar/job_fixtar_E605_N2LL.sh

sbatch Jobs_fixtar/job_fixtar_E772_N3LL.sh
sbatch Jobs_fixtar/job_fixtar_E772_N2LL.sh
