/*
 authors: Valerio Bertone: valerio.bertone@cern.ch
          Chiara Bissolotti
          Andrea Signori
*/


#include "NangaParbat/DWS.h"
#include "NangaParbat/PV17.h"
#include "NangaParbat/PV19.h"
#include "NangaParbat/fastinterface.h"
#include "NangaParbat/convolutiontable.h"
#include "NangaParbat/bstar.h"

#include <fstream>
#include <apfel/timer.h>
#include <sys/stat.h>
#include <cstring>


//------------------------------------------------------------------------------
// Main program
int main(int argc, char* argv[])
{
  // Check that the input is correct otherwise stop the code
  if(argc < 5 || strcmp(argv[1], "--help") == 0)
    {
      std::cout << std::endl;
      std::cout << "Invalid Parameters:" << std::endl;
      std::cout << "Syntax: ./HessianPDFs <1 - configuration file> <2 - path to data folder> <3 - tables folder> <4 - output>" << std::endl;
      std::cout << std::endl;
      exit(-10);
    }

  // Open the configuration file
  YAML::Node config = YAML::LoadFile(argv[1]);

  //Retrieve the PerturbativeOrder
  std::string pertord = config["PerturbativeOrder"].as<std::string>();
  std::string stringord = "please specify";
  if (pertord == "1")
    stringord = "NLL";
  if (pertord == "2")
    stringord = "NNLL";
  if (pertord == "3")
    stringord = "N3LL";
  if (pertord == "-1")
    stringord = "NLLp";
  if (pertord == "-2")
    stringord = "NNLLp";
  // retrieve cutParam to compute qT/Q cut for SIDIS
  //std::vector<double> cutParam = config["cutParam"].as<std::vector<double>>();
  //std::cout << "cutParam[0] = " << cutParam[0] << std::endl;
  //std::cout << "cutParam[1] = " << cutParam[1] << std::endl;
  //std::cout << "cutParam[2] = " << cutParam[2] << std::endl;

  // Open "datasets_*.yaml" file that contains the list of experiments for which we need predictions
  const YAML::Node datasets = YAML::LoadFile(std::string(argv[2]) + "/datasets_xsec_combined.yaml");
  std::cout << "ciao 1" << std::endl;
  //
  //
  // number of files present in the PDF folder
  // MMHT2014*lo68cl: the number of hessian members is 50 + 1 (the 0th member is the central value)
  const int nhesm = 51; //7;

  // Allocate "Parameterisation" derived object
  //NangaParbat::DWS NPFunc{};
  NangaParbat::PV17 NPFunc{};
  //NangaParbat::PV19 NPFunc{};

  const std::string NPfuncName = NPFunc.GetName();

  // Non-perturbative function
  auto const fNP = [&] (double const& x, double const& b, double const& zeta, int const& ifun) -> double{ return NPFunc.Evaluate(x, b, zeta, ifun); };
  std::cout << "ciao 2" << std::endl;
  // Now read convolution tables and compute predictions from the grids
  for (auto const& exp : datasets)
    for (auto const& ds : exp.second)
      {
        // Fill DataHandler object dh
        const std::string datafile = std::string(argv[2]) + "/" + exp.first.as<std::string>() + "/" + ds["file"].as<std::string>();
        const NangaParbat::DataHandler dh{ds["name"].as<std::string>(), YAML::LoadFile(datafile)};

        // Get values of qT and experimental values for the experiment considered in the loop
        const std::vector<double> qT = dh.GetKinematics().qTv;
        const std::vector<double> mean = dh.GetMeanValues();
        //const std::pair<double, double> Qint = dh.GetKinematics().var1b;
        //std::cout << Qint.first << std::endl;
        std::cout << "ciao 3" << std::endl;
        // Fill gc vector with the prediction from each hessian table for the experiment considered in the loop
        std::vector<std::vector<double>> gc;
        for (size_t i = 0; i < nhesm ; i++)
        {
          const std::string table = std::string(argv[3]) + "/" + ds["name"].as<std::string>() + "_" + std::to_string(i) + ".yaml";
          //const NangaParbat::ConvolutionTable ct{YAML::LoadFile(table), cutParam}; //good both for SIDIS and DY
          const NangaParbat::ConvolutionTable ct{YAML::LoadFile(table)}; // sufficient for DY
          gc.push_back(ct.GetPredictions(fNP));
        }
        std::cout << "ciao 4" << std::endl;
        // Apply the Hessian formula
        std::vector<double> dgc(gc[0].size(), 0.); // initializing vector for the errors DX - size of the number of the points, all elements initialized at zero
        std::vector<double> relerr(gc[0].size(), 0.); // relative hessian error
        for (int j = 0; j < (int) gc[0].size(); j++) // j index on the qT points
          {
            for (int i = 1; i < (int) gc.size() / 2; i++) // i index on the hessain sets: sum over *pairs* of hessian sets
              dgc[j] += pow(gc[2*i-1][j] - gc[2*i][j], 2);
            dgc[j] = sqrt(dgc[j]) / 2;
            relerr[j] = dgc[j] / gc[0][j];
          }

        // Create directory
        std::string path = std::string(argv[4]) + "/" + NPfuncName;
        mkdir(path.c_str(), ACCESSPERMS);

        //Print on file.out
        std::ofstream newdatafile(path + "/" + ds["name"].as<std::string>() + ".out", std::ios::out);
        newdatafile << "# Dataset name: " << ds["name"].as<std::string>() << "\t"
                    //<< " Non pert.func. = " << NPfuncName
                    << std::endl;
        newdatafile << "#\t"
                    << "  qT [GeV]   \t"
                    << "   pred.     \t"
                    << "    exp.     \t"
                    //<< " hes.abs.err \t" // unreliable when calculated with tables w/o integrations (see the docs)
                    << "  hes.err(%) \t"
                    << "  hes.err(%) * exp \t"
                    << std::endl;

        for (int i = 0; i < (int) dgc.size(); i++)
          {
            // print zero (and not 'nan') if the prediction doesn't exist or it's negative
            if (gc[0][i] <=  0)
              {
              //  gc[0][i] =  0;
              //  dgc[i] = 0;
              //  relerr[i] = 0;
              }
              newdatafile << std::scientific;
              newdatafile << i << "\t"
                          << (dh.GetKinematics().IntqT ? ( qT[i] + qT[i+1] ) / 2 : qT[i]) << "\t"
                          << gc[0][i] << "\t"
                          << mean[i]  << "\t"
                          //<< dgc[i]   << "\t" // unreliable when calculated with tables w/o integrations (see the docs)
                          << relerr[i] << "\t"
                          << relerr[i] * mean[i] << "\t"
                          << std::endl;
          }
        newdatafile.close();

        // Compute number of significant predictions (to plot)
        int npred = 0; // number of predictions different from zero, = number of considered qT points
        for (int i = 0; i < (int) dgc.size(); i++)
            if ( gc[0][i] >  0)
              npred++ ;


        //Print on terminal
        std::cout << "# Dataset name: " << ds["name"].as<std::string>() << "\t"
                  << " Non pert.func. = " << NPfuncName << "\t"
                  << " pert. ord. = " << stringord
                  << std::endl;
        std::cout << "#\t"
                  << "  qT [GeV]   \t"
                  << "   pred.     \t"
                  << "    exp.     \t"
                  //<< " hes.abs.err \t" // unreliable when calculated with tables w/o integrations (see the docs)
                  << "  hes.err(%) \t"
                  << "  hes.err(%) * exp \t"
                  << std::endl;

        for (int i = 0; i < (int) dgc.size(); i++)
          {
            //if (gc[0][i] > 0)
            //{
              std::cout << std::scientific;
              std::cout << i << "\t"
                        << (dh.GetKinematics().IntqT ? ( qT[i] + qT[i+1] ) / 2 : qT[i]) << "\t"
                        << gc[0][i] << "\t"
                        << mean[i]  << "\t"
                        //<< dgc[i]   << "\t" // unreliable when calculated with tables w/o integrations (see the docs)
                        << relerr[i] << "\t"
                        << relerr[i] * mean[i] << "\t"
                        << std::endl;
            //}
          }

      }
      
  return 0;
}
